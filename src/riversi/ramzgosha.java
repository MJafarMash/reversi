package riversi;

import java.util.*;

public class ramzgosha {

    private Scanner in = new Scanner(System.in);
    int[][] gameBorard = new int[8][8];
    int tedadwhite;
    int tedadblak;
    int count;
    int turn;
    static final int BLACK = 1;
    static final int WHITE = -1;
    public boolean doAI = false;

    public ramzgosha() {
        newGame();
    }

    public void play() {
        while (count <= 64) {
            System.out.println("toole khane morede nazar ra vared konid: ");
            int i = in.nextInt();
            System.out.println("arze khane morede nazar ra vared konid: ");
            int j = in.nextInt();
            playNextTurn(i, j);
        }
        if (tedadblak > tedadwhite) {
            System.out.println("Black Win!");
        } else if (tedadblak < tedadwhite) {
            System.out.println("White Win!");
        } else {
            System.out.println("No one win!");
        }
    }

    private void doChecks() {
        boolean check = false;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (isAvailable(i, j)) {
                    check = true;
                    break;
                }
            }
        }
        if (check == false) {
            if (turn == BLACK) {
                turn = WHITE;
            } else {
                turn = BLACK;
            }
            if (turn == BLACK) {
                System.out.println("Next Player is Black");
            } else {
                System.out.println("Next Player is White");
            }
        }
        
        Riversi.gameBoard.syncWithEngine();
        
        if (count == 64) {
            Riversi.gameBoard.win();
        }
    }
    
    class Tuple {
        public int i,j;

        public Tuple(int i, int j) {
            this.i = i;
            this.j = j;
        }
        
    }
    
    private void doAI() {
        Set<Tuple> availables = new HashSet<>();
        for (int i = 0; i < 8; ++i) {
            for(int j = 0; j < 8; ++j) {
                if (gameBorard[i][j] == 0) {
                    if (isAvailable(i, j)) {
                        availables.add(new Tuple(i,j));
                    }
                }
            }
        }
        
        Tuple MaxTuple = null;
        int maxNum = -1;
        Tuple c;
        Iterator<Tuple> itr = availables.iterator();
        while (itr.hasNext()) {
            c = itr.next();
            if (MaxTuple == null) {
                MaxTuple = c;
                maxNum = getNumberOfReverses(c.i, c.j);
                continue;
            }
            int r = getNumberOfReverses(c.i, c.j);
            if (r > maxNum) {
                MaxTuple = c;
                maxNum = r;
            }
        }
        
        playNextTurn(MaxTuple.i, MaxTuple.j, false);
    }
    public void playNextTurn(int i, int j) {
        playNextTurn(i, j, true);
    }
    
    public void playNextTurn(int i, int j, boolean fromClick) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Riversi.gameBoard.jAI.setEnabled(false);
            }
        });
        if (doAI && fromClick && turn == BLACK) {
            return;
        }
        if (isAvailable(i, j)) {
            changeColor(i, j);
            count = count + 1;
            if (turn == BLACK) {
                turn = WHITE;
            } else {
                turn = BLACK;
            }
        }
        if (turn == BLACK) {
            System.out.println("Next Player is black");
        } else {
            System.out.println("Next Player is White");
        }
        for (int x = 0; x < gameBorard.length; x++) {
            for (int y = 0; y < gameBorard.length; y++) {
                if (gameBorard[x][y] == BLACK) {
                    System.out.print("B ");
                } else if (gameBorard[x][y] == WHITE) {
                    System.out.print("W ");
                } else {
                    System.out.print("0 ");
                }
                // System.out.print(gameBorard[x][y] + "  ");
            }
            System.out.println();
        }
        doChecks();
        if (doAI && turn == BLACK) {
            Riversi.gameBoard.setThink(true);
            new Timer().schedule(new TimerTask() {

                @Override
                public void run() {
                    doAI();
                    Riversi.gameBoard.setThink(false);
                }
            }, 2000);
        }
    }

    public boolean isAvailable(int row, int col) {
        if (gameBorard[row][col] != 0) {
            return false;
        }

        // Check horizontal - chap
        for (int i = col - 1; i >= 0; --i) {
            // agar rah peyda kard return true
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    return true;
                } else {
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                break;
            }
        }
        // rast
        for (int i = col + 1; i < 8; ++i) {
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    return true;
                } else {
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                break;
            }
        }
        // bala
        for (int i = row - 1; i >= 0; --i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    return true;
                } else {
                    break;
                }
            } else {
                if (gameBorard[i][col] == 0) {
                    break;
                }

            }
        }
        // paiini
        for (int i = row + 1; i < 8; ++i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    return true;
                } else {
                    break;
                }
            } else if (gameBorard[i][col] == 0) {
                break;
            }
        }

        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row + k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        return true;
                    } else {
                        break;
                    }
                } else if (gameBorard[row + k][col + k] == 0) {
                    break;
                }
            }
        }
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row - k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        return true;
                    } else {
                        break;
                    }
                } else if (gameBorard[row - k][col - k] == 0) {
                    break;
                }
            }
        }
        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row + k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        return true;
                    } else {
                        break;
                    }
                } else if (gameBorard[row + k][col - k] == 0) {
                    break;
                }
            }
        }
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row - k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        return true;
                    } else {
                        break;
                    }
                } else if (gameBorard[row - k][col + k] == 0) {
                    break;
                }
            }
        }
        return false;
    }

    public int getNumberOfReverses(int row, int col) {
        int n = 0;
        
        boolean shouldChange;

        shouldChange = false;
        for (int i = col - 1; i >= 0; --i) {
            // agar rah peyda kard return true
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row][col - i] != turn) {
                //gameBorard[row][col - i] = turn;
                ++n;
                ++i;
            }
        }
        shouldChange = false;
        for (int i = col + 1; i < 8; ++i) {
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row][col + i] != turn) {
                ++n;
                ++i;
            }
        }
        shouldChange = false;
        for (int i = row - 1; i >= 0; --i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else {
                if (gameBorard[i][col] == 0) {
                    shouldChange = false;
                    break;
                }

            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col] != turn) {
                ++n;
                ++i;
            }
        }
        shouldChange = false;
        for (int i = row + 1; i < 8; ++i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[i][col] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col] != turn) {
                ++n;
                ++i;
            }
        }

        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row + k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row + k][col + k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col + i] != turn) {
                ++n;
                ++i;
            }
        }

        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row - k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row - k][col - k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col - i] != turn) {
                ++n;
                ++i;
            }
        }
        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row + k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row + k][col - k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col - i] != turn) {
                ++n;
                ++i;
            }
        }
        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row - k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row - k][col + k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col + i] != turn) {
                ++n;
                ++i;
            }
        }
        ++n;
        
        return n;
    }
    
    public void changeColor(int row, int col) {
        boolean shouldChange;

        shouldChange = false;
        for (int i = col - 1; i >= 0; --i) {
            // agar rah peyda kard return true
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row][col - i] != turn) {
                gameBorard[row][col - i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        shouldChange = false;
        for (int i = col + 1; i < 8; ++i) {
            if (gameBorard[row][i] == turn) {
                if (Math.abs(i - col) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[row][i] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row][col + i] != turn) {
                gameBorard[row][col + i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        shouldChange = false;
        for (int i = row - 1; i >= 0; --i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else {
                if (gameBorard[i][col] == 0) {
                    shouldChange = false;
                    break;
                }

            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col] != turn) {
                gameBorard[row - i][col] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        shouldChange = false;
        for (int i = row + 1; i < 8; ++i) {
            if (gameBorard[i][col] == turn) {
                if (Math.abs(i - row) > 1) {
                    shouldChange = true;
                    break;
                } else {
                    shouldChange = false;
                    break;
                }
            } else if (gameBorard[i][col] == 0) {
                shouldChange = false;
                break;
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col] != turn) {
                gameBorard[row + i][col] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }

        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row + k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row + k][col + k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col + i] != turn) {
                gameBorard[row + i][col + i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }

        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row - k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row - k][col - k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col - i] != turn) {
                gameBorard[row - i][col - i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row + k && row + k < 8 && 0 <= col - k && col - k < 8) {
                if (gameBorard[row + k][col - k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row + k][col - k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row + i][col - i] != turn) {
                gameBorard[row + i][col - i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        shouldChange = false;
        for (int k = 1; k < 8; ++k) {
            if (0 <= row - k && row - k < 8 && 0 <= col + k && col + k < 8) {
                if (gameBorard[row - k][col + k] == turn) {
                    if (Math.abs(k) > 1) {
                        shouldChange = true;
                        break;
                    } else {
                        shouldChange = false;
                        break;
                    }
                } else if (gameBorard[row - k][col + k] == 0) {
                    shouldChange = false;
                    break;
                }
            }
        }
        if (shouldChange) {
            int i = 1;
            while (gameBorard[row - i][col + i] != turn) {
                gameBorard[row - i][col + i] = turn;
                ++i;
                if (turn == BLACK) {
                    ++tedadblak;
                    --tedadwhite;
                } else {
                    ++tedadwhite;
                    --tedadblak;
                }
            }
        }
        gameBorard[row][col] = turn;
        if (turn == WHITE) {
            tedadwhite++;
        } else {
            tedadblak++;
        }
    }

    public void newGame() {
        for (int i = 0; i < gameBorard.length; i++) {
            for (int j = 0; j < gameBorard.length; j++) {
                gameBorard[i][j] = 0;
            }
        }
        gameBorard[3][3] = BLACK;
        gameBorard[4][4] = BLACK;
        gameBorard[3][4] = WHITE;
        gameBorard[4][3] = WHITE;
        tedadwhite = 2;
        tedadblak = 2;
        count = 4;
        turn = WHITE;
    }
}